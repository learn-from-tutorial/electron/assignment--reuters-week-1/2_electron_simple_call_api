'use strict'
// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')

require('electron-reload')(__dirname)

// Run the following from the Console tab of your app's DevTools
// require('devtron').install()
// You should now see a Devtron tab added to the DevTools

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let messages = []

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })
  console.log("create main window")

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  mainWindow.webContents.openDevTools()


  let rendererWindow
  // create renderer window
  ipcMain.on('renderer-window', () => {
    // if rendererWindow does not already exist
    if (!rendererWindow) {
      // create a new renderer window
      rendererWindow = new BrowserWindow({
        // file: path.join(__dirname, 'renderer.html'),
        width: 600,
        height: 400,
        // close with the main window
        parent: mainWindow,
        webPreferences: {
          nodeIntegration: true
        }
      })     
      // and load the renderer.html of the app.
      rendererWindow.loadFile('renderer.html')

      rendererWindow.once('ready-to-show', () => {
        rendererWindow.show()
      })

      // cleanup
      rendererWindow.on('closed', () => {
        rendererWindow = null
      })
    }
  })

  // create movie window
  let movieWindow
  ipcMain.on('movie-window', () => {
    // if movieWindow does not already exist
    if (!movieWindow) {
      // create a new renderer window
      movieWindow = new BrowserWindow({
        // file: path.join(__dirname, 'renderer.html'),
        width: 600,
        height: 400,
        // close with the main window
        parent: mainWindow,
        webPreferences: {
          nodeIntegration: true
        }
      })     
      // and load the movies.html of the app.
      movieWindow.loadFile(path.join('movies', 'movies.html'))

      movieWindow.once('ready-to-show', () => {
        movieWindow.show()
      })

      // cleanup
      movieWindow.on('closed', () => {
        movieWindow = null
      })
    }
  })

  // add message from renderer window
  ipcMain.on('tell-message-to-main', (event, newMessage) => {
    messages = [ ...messages, newMessage ]
    // send ไปที่ตัว index renderer
    mainWindow.send('recieve-message', messages)
    // คำถาม webContent.send ต่างจาก send อย่างไร เพราะผลลัพธ์มันออกมาเหมือนกัน
    // mainWindow.webContent.send('recieve-message', messages) 

    // send ไปที่ตัว renderer renderer
    rendererWindow.send('recieve-message', messages)
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// ref: https://electronjs.org/docs/api/app
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// Listen instance for quit.
app.on('before-quit', function(event) {
  console.log("App about to Quit")
  // event.preventDefault()
})

// Listen for app blur 
// Testing this listener worked is click out of the window app(เทสโดยการคลิกไปข้างนอก window หรือ สลับโปรแกรม หรือพักหน้าจอ)
app.on('browser-window-blur', function (event) {
  console.log("Window out of Focus")
})

// Listen for app focus (user clicks in area of this app window)
app.on('browser-window-focus', function (event) {
  console.log("Window in Focus")
})

// Path in this computer.
console.log("Desktop path -> ", app.getPath('desktop'))
console.log("Music path -> ", app.getPath('music'))
console.log("Temp path -> ", app.getPath('temp'))
console.log("User data path -> ", app.getPath('userData'))

// Set app icon badge count (จำนวนตัวเลขแจ้งเตือนบนไอคอนแอพ)
// work for unity dock on Linux and dock on Mac os  
app.setBadgeCount(22)


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


