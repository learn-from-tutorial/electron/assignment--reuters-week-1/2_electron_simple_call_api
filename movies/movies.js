'use strict'

const { ipcRenderer } = require('electron')
const axios = require('axios')

let movies = []
axios.defaults.headers.post['Content-Type'] = 'application/json'

const fetchMovies = () => {
    axios.get('http://localhost:8010/api/movies')
        .then (res => {
            movies = res.data
            console.log("movies: ", movies)
            createMoviesHtml()
        })
        .catch (err => {
            console.error(err.response)
        })
}

const createMoviesHtml = () => {
    const moviesList = document.getElementById('moviesList')

        let html = ''
        movies.forEach(movie => {
            html = html + ` 
            <div class="column is-one-quarter has-margin-bottom-3">
                    <img src="${movie.images.poster}">
                    <strong>
                        ${movie.title}
                    </strong>
            </div>`
        })
      
        if (typeof html !== 'undefined' || html !== '') {
            // set value in html tag
            moviesList.innerHTML = html
        }
}


document.addEventListener('DOMContentLoaded', (event) => {
    // call movies api
    fetchMovies()
})