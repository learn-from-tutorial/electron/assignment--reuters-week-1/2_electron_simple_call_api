// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

'use strict'
const { ipcRenderer } = require('electron')

// create add renderer window button
document.getElementById('tellRendererFromMainBtn').addEventListener('click', () => {
    ipcRenderer.send('renderer-window')
})

// on receive message
ipcRenderer.on('recieve-message', (event, messages) => {
    // get the messages ul
    const rendererMessageList = document.getElementById('rendererMessageList')

    let html = ''
    messages.forEach(message => {
        html = html + `<li>${message}</li>`
    })
  
    if (typeof html !== 'undefined' || html !== '') {
        // set value in html tag
        rendererMessageList.innerHTML = html
    }

})

// movies section
document.getElementById('openMovieWindowBtn').addEventListener('click', () => {
    ipcRenderer.send('movie-window')
})
