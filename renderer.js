'use strict'

const { ipcRenderer } = require('electron')

document.getElementById('rendererToMainBtn').addEventListener('click', () => {
    console.log("rendererToMainBtn clicked!")
    
    const rendererToMainTextBox = document.getElementById('rendererToMainTextBox')

    // send message to main process
    ipcRenderer.send('tell-message-to-main', rendererToMainTextBox.value)

    // reset input
    rendererToMainTextBox.value = ''
})

// on receive message
ipcRenderer.on('recieve-message', (event, messages) => {
    // get the messages ul
    const rendererMessageList = document.getElementById('rendererMessageList')

    let html = ''
    messages.forEach(message => {
        html = html + `<li>${message}</li>`
    })
  
    if (typeof html !== 'undefined' || html !== '') {
        // set value in html tag
        rendererMessageList.innerHTML = html
    }

})
